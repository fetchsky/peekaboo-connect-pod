//
//  Peekaboo_SDK.h
//  Peekaboo
//
//  Created by Zain Sajjad on 18/07/2018.
//  Copyright © 2018 Fetch Sky. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Peekaboo_SDK.
FOUNDATION_EXPORT double Peekaboo_SDKVersionNumber;

//! Project version string for Peekaboo_SDK.
FOUNDATION_EXPORT const unsigned char Peekaboo_SDKVersionString[];

@interface PeekabooConnect : NSObject
@property (nonatomic,strong) UIViewController *parent;

- (void)initializePeekaboo;

- (UIViewController *)getPeekabooUIViewController:(NSDictionary*)params;

@end
