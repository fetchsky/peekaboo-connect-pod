Pod::Spec.new do |s|
  s.name           = "PeekabooConnect"
  s.version        = "2.3.177"
  s.summary        = "Empowering Digital Experiences"
  s.description    = "Digital Experiences for fintech"
  s.license        = "Apache 2.0"
  s.author         = "Peekaboo Guru"
  s.homepage       = "https://gitlab.com/fetchsky/peekaboo-connect-pod"
  s.platform       = :ios, "10.0"
  s.source         = { :git => "https://gitlab.com/fetchsky/peekaboo-connect-pod.git", :branch => "master" }
  # s.source_files   = "*.{h,m}"
  s.requires_arc   = true
  s.vendored_frameworks = "PeekabooConnect.xcframework"
  s.resource       = "PeekabooConnect.bundle"
end
